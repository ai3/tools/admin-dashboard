import { PrometheusDriver } from 'prometheus-query';

const prom = new PrometheusDriver({
    endpoint: "https://admin.autistici.org",
    baseURL: "/api/v1",
    withCredentials: true
});

const accountsQuery = "sum(max(accounts_count) by (shard,type,status)) by (type)";

const alertsURL = "https://admin.autistici.org/alerts.json";

const alertsQuery = {
    filters: ["@state=active", "severity=page"],
    gridLabel: "",
    gridSortReverse: false,
    gridLimits: {},
    sortOrder: "",
    sortLabel: "",
    sortReverse: false,
    defaultGroupLimit: 5,
    groupLimits: {}
};

const accountNiceNames = {
    mail: "email accounts",
    web: "websites",
    lists: "lists",
    newsletters: "newsletters"
};

function queryAccounts(el) {
    prom.instantQuery(accountsQuery)
        .then((res) => {
            const series = res.result;
            var tmp = [];
            series.forEach((serie) => {
                tmp.push('' + serie.value.value + ' ' +
                         accountNiceNames[serie.metric.labels.type]);
            });

            var txt = 'We are currently hosting ' + tmp.join(', ') + '.';
            var p = document.createElement('p');
            p.appendChild(document.createTextNode(txt));
            el.append(p);
        })
        .catch((err) => {
            dashboardError('Failed to fetch account metrics', err);
        });
}

function queryAlerts(el) {
    fetch(alertsURL, {
        method: 'POST',
        body: JSON.stringify(alertsQuery),
        credentials: 'include'
    })
        .then((resp) => {
            if (!resp.ok) {
                throw new Error('HTTP request failed');
            }
            return resp.json();
        })
        .then((result) => {
            if (result.status != "success") {
                throw new Error('JSON API request failed');
            }
            var numFiring = result.totalAlerts;

            var txt = 'No active alerts.';
            if (numFiring > 0) {
                txt = '' + numFiring + ' active alerts!';
            }
            var p = document.createElement('p');
            p.appendChild(document.createTextNode(txt));
            el.append(p);            
        })
        .catch((err) => {
            dashboardError('Failed to fetch active alerts', err);
        });
}

function dashboardError(msg, err) {
    var p = document.createElement('p');
    var errmsg = err.error;
    if (!errmsg) {
        errmsg = err.message;
    }
    p.appendChild(document.createTextNode(msg + ': ' + errmsg));
    document.getElementById('errors').append(p);
}

window.addEventListener('load', () => {
    console.log('Populating data items...');

    queryAccounts(document.getElementById('accountInfo'));
    queryAlerts(document.getElementById('alertInfo'));
});
