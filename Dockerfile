FROM golang:1.23 AS build

ADD . /src
WORKDIR /src
RUN go build -tags netgo -o server server.go

FROM node:current-bullseye AS assets

ADD . /src
WORKDIR /src
RUN npm install && ./node_modules/.bin/webpack

FROM scratch
COPY --from=assets /src/assets/ /var/www/
COPY --from=build /src/server /server

ENTRYPOINT ["/server"]
