Simple admin dashboard for A/I.

Contains links to useful internal systems and account management tools.

The HTTP server serves static files but also proxies requests to some
backends we're interested in (prometheus, alertmanager). This is
necessary because we can't do client-side requests to these other
services on other domains due to browser's CORS policies: in practice,
CORS prevents the single-sign on request flow (through the sso-server
and back) to work because redirects are [forbidden by
CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSExternalRedirectNotAllowed).


