const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const PurgeCSSPlugin = require('purgecss-webpack-plugin');
const { SubresourceIntegrityPlugin } = require('webpack-subresource-integrity');

module.exports = {
    context: __dirname + '/src',
    entry: {
        dashboard: [
            './dashboard.js',
            //'./dashboard.css',
        ],
    },
    mode: 'production',
    output: {
        publicPath: '/',
        path: __dirname + '/assets',
        filename: '[name]-[contenthash].min.js',
        assetModuleFilename: 'assets/[name][ext]',
        crossOriginLoading: 'anonymous',
        clean: true,
    },
    plugins: [
        /**
        new MiniCssExtractPlugin({
            filename: '[name]-[contenthash].css',
        }),
        **/
        new HtmlWebpackPlugin({
            template: 'index.html',
            minify: false,
            inject: false,
        }),
        /**
        new PurgeCSSPlugin({
            paths: [__dirname + '/src/index.html'],
            safelist: [/^oi/, /show/, /tooltip/],
        }),
        **/
        new SubresourceIntegrityPlugin(),
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.(ico|png|gif|jpe?g|svg|eot|otf|ttf|woff)$/,
                type: 'asset/resource',
            },
        ],
    },
};
